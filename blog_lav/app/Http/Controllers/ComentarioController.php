<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Comentario;

class ComentarioController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	    $request->validate([
		    'comentario' => 'required|max:500'
	    ]);

	    $comentario = new Comentario();
	    $comentario['comentario'] = $request->get('comentario');
	    $comentario['post'] = $request->get('post_id');
	    $comentario['data_comentario'] = Carbon::now();
	    $comentario->save();

	    return redirect()->back()->with('Sucesso', 'Comentario criado, obrigado pela contribuição :)');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
	    Comentario::where('comentario_id', $id)->delete();
	    return redirect()->back()->with('Sucesso', 'Comentario deletado :(');
    }
}
