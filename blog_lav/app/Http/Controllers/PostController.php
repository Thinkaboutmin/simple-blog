<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;
use Carbon\Carbon;
use App\Comentario;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $posts = Post::orderBy('data_postagem', 'desc')->paginate(5);

	    return view('posts.index', compact('posts'))
		    ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
	    if (!Auth::check())
	    {
		    return redirect()->route('login');
	    }

	    return view('posts.criar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            if (!Auth::check())
	    {
		    return redirect()->route('login');
	    }
	    
	    $request->validate([
		    'titulo' => 'required|max:70',
		    'conteudo' => 'required'
	    ]);
	    $post = new Post();
	    $post['titulo'] = $request->get('titulo');
	    $post['conteudo'] = $request->get('conteudo');
	    $post['autor'] = Auth::id();
	    $post['data_postagem'] = Carbon::now();
	    
	    $post->save();

	    return redirect()->route('posts.index')->with('Sucesso', 'Post criado :)');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	    $post = Post::find($id);
	    $comentarios = Comentario::where('post', $id)->orderBy('data_comentario', 'desc')->get();
	    return view('posts.mostrar')
		    ->with('post', $post)
	    	    ->with('comentarios', $comentarios);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    if (!Auth::check())
	    {
		    return redirect()->route('login');
	    }

	    $post = Post::find($id);
	    return view('posts.editar', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	    if (!Auth::check())
	    {
		    return redirect()->route('login');
	    }            

            $request->validate([
		    'titulo' => 'required|max:70',
		    'conteudo' => 'required'
	    ]);

	    Post::find($id)->update($request->all());

	    return redirect()->route('posts.index')->with('Sucesso', 'Post atualizado :)');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
	    if (!Auth::check())
	    {
		    return redirect()->route('login');
	    }

	    Post::find($id)->delete();
	    return redirect()->route('posts.index')->with('Sucesso', 'Post deletado :(');
    }
}
