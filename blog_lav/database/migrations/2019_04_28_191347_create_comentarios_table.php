<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComentariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentarios', function (Blueprint $table) {
	    $table->bigIncrements('comentario_id');
	    $table->string('comentario', 500);
	    $table->integer('post')->unsigned();
            $table->dateTime('data_comentario');
	});

	Schema::table('comentarios', function(Blueprint $table) {
	    $table->foreign('post')->references('id')->on('posts')->onDelete('cascade');
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cometarios');
    }
}
