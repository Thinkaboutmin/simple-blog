@extends('layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('posts.index') }}"> Voltar</a>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <h1>{{ $post->titulo}}</h1>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
	    <div class="form-group">
                <pre style="background-color: white;white-space:pre-wrap;">{{ $post->conteudo}}</pre>
            </div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 text-right">
            <div class="form-group">
                <strong>Data da postagem:</strong>
                {{ $post->data_postagem}}
            </div>
	</div>
	<br>

	<div class="col-xs-12 col-sm-12 col-md-12">
	    @if ($message = Session::get('Sucesso'))
            <div class="alert alert-success">
           	<p>{{ $message }}</p>
            </div>
            @endif
            <div class="form-group">
		<h3><strong>Comentarios:</strong><h3>
			<div class="widget-area no-padding blank" style="margin-bottom: 40px">
				<div class="status-upload">
						<form action="{{route('comentarios.store')}}" method="POST">
							{{csrf_field()}}
							<input type="hidden" value="{{$post->id}}" name="post_id">
							<textarea placeholder="Qual a sua opnião? Nos conte!" name="comentario" ></textarea>
							<button type="submit" class="btn btn-success green form-control"><i class="fa fa-share"></i>Comentar!</button>
						</form>
					</div>
				</div>
			</div>
		@foreach ($comentarios as $comentario)
		<div class="container">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<strong>Visitante</strong> <span class="text-muted">Comentado em: {{$comentario->data_comentario}}</span>
						@auth
						<form method="post" action="{{route('comentarios.destroy', $comentario->comentario_id)}}" style="display:inline;">
							{{csrf_field()}}
							{{ method_field('DELETE') }}
							<span class="text-right clearfix"><input type="submit" class="btn btn-danger clearfix" value="Apagar" style="float:right;" onclick="return confirm('Deseja mesmo deletar esse comentario?')"></span>
						</form>
						@endauth
					</div>
					<div class="panel-body">
						<pre style="background-color: transparent; border: 0px">{{$comentario->comentario}}</pre>
					</div>
				</div>
			</div>
	       </div>	
		@endforeach
            </div>
	</div>
    </div>
@endsection
