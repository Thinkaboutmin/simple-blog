@extends('layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
	    <div class="pull-left">
		@auth
		<h2>Todas as publicações</h2>
		@endauth
		@guest
		<h2>Publicações<h2>
		@endguest
	    </div>
	    @auth
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('posts.create') }}">Criar novo post</a>
	    </div>
	    @endauth

        </div>
    </div>


    @if ($message = Session::get('Sucesso'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <table class="table table-bordered">
	<tr>
	    @auth
	    <th>Id</th>
	    @endauth
            <th>Título</th>
            <th>Data de postagem</th>
            <th width="280px"></th>
        </tr>
    @foreach ($posts as $post)
    <tr>
        @auth
	<td>{{ $post->id }}</td>
	@endauth
        <td>{{ $post->titulo}}</td>
        <td>{{ $post->data_postagem}}</td>
        <td>
	    <a class="btn btn-info" href="{{ route('posts.show',$post->id) }}">Visualizar</a>
	    @auth
	    <a class="btn btn-primary" href="{{ route('posts.edit',$post->id) }}">Editar</a>

	    <form method="post" action="{{route('posts.destroy', $post->id)}}" style="display:inline">
		{{csrf_field()}}
		{{ method_field('DELETE') }} 
	    	<input type="submit" class="btn btn-danger" value="Deletar" onclick="return confirm('Deseja mesmo deletar esse post?')">
	    </form>
	    @endauth
        </td>
    </tr>
    @endforeach
    </table>
{!!$posts->links()!!}

@endsection
