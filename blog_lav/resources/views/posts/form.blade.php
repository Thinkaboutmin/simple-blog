{{csrf_field()}}
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
	    <strong>Título:</strong>
	    <input type="text" class="form-control" placeholder="Título" id="titulo" name="titulo" value="@isset($post->titulo){{$post->titulo}}@endisset">
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
	    <strong>Conteúdo:</strong>
	    <textarea class="form-control" id="conteudo" name="conteudo" placeholder="Digite o conteúdo" style="height:300px">@isset($post->conteudo){{$post->conteudo}}@endisset</textarea>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Publicar</button>
    </div>
</div>
