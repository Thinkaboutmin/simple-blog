@extends('layout')

@section('content')

<div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Posts</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('/') }}"> Voltar</a>
            </div>
        </div>
    </div>


    <div class="row">
	@foreach($posts as $post)
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Título:</strong>
                {{ $post->titulo}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Conteúdo:</strong>
                {{ $post->conteudo}}
            </div>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Data da postagem:</strong>
                {{ $post->data_postagem}}
            </div>
	</div>
	@endforeach
    </div>

@endsection
