# Simple Blog

Um simples blog para aqueles que gostam de ascii feito em Laravel 5.5 LTS.

## Instruções

O banco de dados, contendo as tabelas preenchidas, estão na pasta blog_backup.
O nome do banco de dados é blog.

Login do administrador<br />
`adm@adm.com`<br />
Senha do administrador<br />
`adminadmin`

Também, é possível criar outro administrador caso queira.

Foi usado o MariaDB como provedor de banco de dados.